<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Country */

$this->title = $country->name;
$this->params['breadcrumbs'][] = ['label' => $country->continent->name, 'url' => ['continent/view', 'code' => $country->continent->code]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container">
    <div class="row">
        <div class="col-sm-6"> <?//country-view ?>
        
            <h1><?= Html::encode($this->title) ?></h1>
        
            <p>
                <?= Html::a('Update', ['update', 'id' => $country->country_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $country->country_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        
            <?= DetailView::widget([
                'model' => $country,
                'attributes' => [
                    'country_id',
                    'code',
                    'name',
                    'official_name',
                    'iso3',
                    'number',
                    'currency',
                    'capital',
                    'area',
                    [
                        'attribute' => 'Flag',
                        'value' => "@web/images/countries/png250px/" .strtolower($country->code). ".png",
                        'format' => ['image', ['width'=>'140', 'height'=>'100']],
                    ],
                    'continent_id',
                    'coords',
                    'display_order',
                ],
            ]) ?>
        </div>
        
        <div class="col-sm-6" id="map" style="height: 400px; margin-top: 113px">
            <?php 
        
                $first_str = strpos($country->coords, ",");
                $lat = substr($country->coords, 1, $first_str - 1);
                $lng = substr($country->coords, $first_str + 1, -1);
                
            ?>
            <script>
                // Initialize and add the map
                function initMap() {
                // The location of Uluru
                    const uluru = { lat: <?= $lat;?>, lng: <?= $lng;?> };
                // The map, centered at Uluru
                    const map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 4,
                        center: uluru,
                    });
                // The marker, positioned at Uluru
                    const marker = new google.maps.Marker({
                        position: uluru,
                        map: map,
                    });
                }
            </script>
            <script
                    src="https://maps.googleapis.com/maps/api/js?&callback=initMap&libraries=&v=weekly$sensors=false"
                    defer
            ></script>
        </div>
    </div>
</div>
