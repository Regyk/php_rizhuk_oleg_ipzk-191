<?php

namespace app\controllers;

use Yii;
use app\models\ContinentSearch;
use app\models\Continent;
use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\WorldForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ContinentController implements the CRUD actions for Continent model.
 */
class ContinentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Continent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContinentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWorldForm()
    {
        $model = new WorldForm();
        $continents = Continent::find()->asArray()->all();
        $continent = null;
        $countries = null;
        $regions = null;
        $cities = null;

        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $continent = Continent::find()->where(['continent_id' => $model->continent_id])->asArray()->one();
            if ($model->continent_id > 0) {
                $countries = Country::find()->where(['continent_id' => $model->continent_id])->asArray()->all();
                
            }
            if($model->country_id > 0){
                    
                $regions = Region::find()
                ->select('region.*, region_language.name_language as name_language')
                ->leftJoin('region_language', '`region_language`.`region_id` = `region`.`region_id`')
                ->where(['region_language.language' => 'en', 'region.country_id' => $model->country_id])
                ->asArray()
                ->all();

            }
            if($model->region_id > 0){

                $cities = City::find()
                ->select('city.*, city_language.name_language as name_language')
                ->leftJoin('city_language', '`city_language`.`city_id` = `city`.`city_id`')
                ->where(['city_language.language' => 'en', 'city.region_id' => $model->region_id])
                ->asArray()
                ->all();
            }
        }

        return $this->render('world-form', [
            'model' => $model,
            'continents' => $continents,
            'continent' => $continent,
            'countries' => $countries,
            'regions' => $regions,
            'cities' => $cities
        ]);
    }

    public function actionList()
    {
        $continents = Continent::find()->asArray()->all();
        return $this->render('list', ['continents' => $continents]);
    }

    /**
     * Displays a single Continent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($code = 'AF')
    {
        $continent = Continent::find()->where(['code' => $code])->one();

        $countriesProvider = new ActiveDataProvider([
            'query' => Country::find()->where(['continent_id' => $continent->continent_id]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('view', [
            'continent' => $continent,
            'countriesProvider' => $countriesProvider,
            //'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Continent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Continent();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->continent_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Continent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->continent_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Continent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Continent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Continent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Continent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
