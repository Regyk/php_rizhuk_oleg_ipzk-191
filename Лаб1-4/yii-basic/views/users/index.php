<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

/*foreach ($users as $user)
{
    echo $user->username . "<br>";
}*/
?>

<h1>Users</h1>

<ul>
<?php foreach($users as $user): ?>
<li>
    <?= Html::encode($user->username) ?>
</li>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>